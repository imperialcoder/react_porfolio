import React from 'react';
import { useState, useContext, useEffect } from 'react';
import type { MouseEventHandler } from 'react';
import styled, { ThemeContext } from 'styled-components';
import { animated, useSpring } from 'react-spring';
import { useHistory } from 'react-router-dom';

const Component = styled.div`
  display: flex;
  flex: 1;
  align-items: center;
  justify-content: center;
  height: 100%;
  user-select: none;
  cursor: pointer;
  margin: 0.25em 0;
  min-width: 5rem;
  width: 100%;
  max-width: max-content;
  padding: 0 0.75rem;

  ${(props) => props.theme.media.phone} {
    flex-direction: column;
    min-width: 0;
    max-width: 100%;
  }
`;

const AnimatedWrapper = styled(animated.div)`
  display: flex;
  justify-content: center;
  align-items: center;

  ${(props) => props.theme.media.phone} {
    flex-direction: column;
  }
`;

const Icon = styled(animated.i)`
  padding-right: 0.5rem;
  ${(props) => props.theme.media.phone} {
    padding-right: 0;
    padding-bottom: 0.15rem;
  }
`;

const Label = styled.div``;

type MenuButtonProps = {
  path?: string;
  label?: string;
  iconClass?: string;
};

const MenuButton = ({
  path = '',
  label = 'hello world!',
  iconClass = 'fas fa-question-circle',
}: MenuButtonProps): JSX.Element => {
  const [clicked, setClicked] = useState(false);
  const [hovering, setHovering] = useState(false);
  const [selected, setSelected] = useState(false);

  const history = useHistory();
  const themeContext = useContext(ThemeContext);

  useEffect(() => {
    if (history.location.pathname === path) {
      setSelected(true);
    }
    history.listen((location) => {
      if (location.pathname === path) {
        setSelected(true);
      } else {
        setSelected(false);
      }
    });
  }, [history, path, setSelected]);

  const mouseEventHandler: MouseEventHandler = (event) => {
    switch (event.type) {
      case 'click':
        history.push(path);
        setClicked(true);
        setTimeout(() => setClicked(false), 250);
        break;
      case 'mouseenter':
        setHovering(true);
        break;
      case 'mouseleave':
        setHovering(false);
        break;
    }
  };

  const componentClickedSpring = useSpring({
    color:
      selected || hovering
        ? selected
          ? themeContext.color.primary[3]
          : themeContext.color.primary[1]
        : themeContext.color.text,
    transform: clicked ? 'scale(0.8)' : 'scale(1)',
    from: { transform: 'scale(1)', color: themeContext.color.text },
  });

  return (
    <Component
      onClick={mouseEventHandler}
      onMouseEnter={mouseEventHandler}
      onMouseLeave={mouseEventHandler}
    >
      <AnimatedWrapper style={componentClickedSpring}>
        <Icon className={iconClass} />
        <Label>{label}</Label>
      </AnimatedWrapper>
    </Component>
  );
};

export default MenuButton;
