import React from 'react';
import styled from 'styled-components';

const Component = styled.div`
  display: flex;
  flex-direction: column;
  min-height: calc(100vh - 7rem);
  padding: 2rem;
  flex: 1;

  ${(props) => props.theme.media.phone} {
    min-height: calc(100vh - 4rem);
    padding: 0.5rem;
  }

  ${(props) => props.theme.media.ultrawide} {
    max-width: 75rem;
    margin: 0 auto;
  }
`;

export type PageSegmentProps = {
  children: React.ReactNode;
  className?: string;
  id?: string;
  style?: any;
};

const PageSegment = ({
  children,
  className,
  id,
  style,
}: PageSegmentProps): JSX.Element => {
  return (
    <Component className={className} id={id} style={style}>
      {children}
    </Component>
  );
};

export default PageSegment;
