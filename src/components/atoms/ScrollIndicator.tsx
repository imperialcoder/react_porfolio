import React from 'react';
import { useState } from 'react';
import styled from 'styled-components';
import { useSpring, animated } from 'react-spring';

const Component = styled(animated.button)`
  position: sticky;
  left: 0;
  right: 0;
  bottom: 0;
  margin: auto;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  width: min-content;
  height: min-content;
  border: 0;
  padding: 0 0.5rem;
  cursor: pointer;
  background: transparent;
  transform: translateY(-2em);
  z-index: 100;
  outline: none;

  ${(props) => props.theme.media.phone} {
    transform: translateY(-1.5em);
  }
`;

const Arrow = styled(animated.i)`
  font-size: 3rem;
  color: ${(props) => props.theme.color.primary[3]};
`;

const ScrollIndicator = (): JSX.Element => {
  const [bobbing, setBobbing] = useState(false);
  const [hovering, setHovering] = useState(false);
  const [clicked, setClicked] = useState(false);

  // todo: make it so when you click, it goes to the next page
  const mouseEventHandler = (event: any) => {
    switch (event.type) {
      case 'click':
        // setClicked((state: boolean) => !state);
        // setTimeout(() => setClicked((state: boolean) => !state), 250);
        break;
      case 'mouseenter':
      case 'mouseleave':
        // setHovering((state: boolean) => !state);
        break;
      default:
        console.error(
          `mouseEventHandler: ${event.type} has no case! Please add a case.`,
        );
        break;
    }
  };

  const focusEventHandler = (event: any) => {
    switch (event.type) {
      case 'focus':
      case 'blur':
        break;
      default:
        console.error(
          `focusEventHandler: ${event.type} has no case! Please add a case.`,
        );
        break;
    }
  };

  // todo: when we upgrade to version 9 of react-spring, replace with while loop using async calls
  const bobbingArrowSpring = useSpring({
    config: { mass: 1, tension: 200, friction: 26 },
    transform: [
      hovering || bobbing ? 'translate(0, 0em)' : 'translate(0, 0.35em)',
      hovering && !clicked ? 'scale(1.5)' : 'scale(1)',
    ].join(' '),
    from: { transform: 'translate(0em, 0em) scale(1)' },
    onRest: () => {
      setBobbing((state: boolean) => !state);
    },
  });

  return (
    <Component
      onClick={mouseEventHandler}
      onMouseEnter={mouseEventHandler}
      onMouseLeave={mouseEventHandler}
      onFocus={focusEventHandler}
      onBlur={focusEventHandler}
    >
      <Arrow style={bobbingArrowSpring} className="fas fa-caret-down" />
    </Component>
  );
};

export default ScrollIndicator;
