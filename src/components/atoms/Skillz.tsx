import React from 'react';
import styled from 'styled-components';

import Section from 'templates/Resume/GridItem';
import SubTitle from 'atoms/SubTitle';
import Paragraph from 'atoms/elements/Paragraph';

const Component = styled(Section)`
  align-items: center;
  ${(props) => props.theme.media.phone} {
    align-items: flex-start;
    grid-column: 1 / span 3;
  }
`;

const Title = styled(SubTitle)`
  text-align: center;

  ${(props) => props.theme.media.phone} {
    text-align: left;
  }
`;

const Info = styled(Paragraph)`
  margin: 0;
  height: 100%;
  text-align: center;
  word-spacing: 1.2rem;
  line-height: 1.5rem;

  ${(props) => props.theme.media.phone} {
    text-align: left;
  }
`;

export type SkillzProps = { title: string; children?: string };

const Skillz = (props: SkillzProps): JSX.Element => (
  <Component>
    <Title>{props.title}</Title>
    <Info>{props.children}</Info>
  </Component>
);

export default Skillz;
