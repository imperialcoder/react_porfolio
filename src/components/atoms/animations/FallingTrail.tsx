import React from 'react';
import { useTrail, animated } from 'react-spring';
import styled from 'styled-components';

const Component = styled.div`
  display: flex;
  flex-wrap: wrap;
  user-select: none;
`;

const TrailItem = styled(animated.div)<{ isSpace: boolean }>`
  width: ${(props) => (props.isSpace ? '1rem' : '')};
`;

export type FallingTrailProps = {
  className?: string;
  style?: any;
  children: React.ReactNode;
  animate?: boolean;
  config?: any;
  delay?: number;
};

const FallingTrail = ({
  className,
  style,
  children,
  animate = true,
  config,
  delay,
}: FallingTrailProps): JSX.Element => {
  let trailItems: string | any[];
  if (typeof children === 'string') {
    trailItems = children
      .split(' ')
      .reduce(
        (prev: any, curr: any, index, array) =>
          index !== array.length - 1
            ? prev.concat(curr, ' ')
            : prev.concat(curr),
        [],
      );
  } else {
    trailItems = children;
  }

  const animation = useTrail(trailItems.length, {
    config: Object.assign(
      { mass: 1, tension: 120, friction: 7, clamp: true },
      config,
    ),
    opacity: animate ? 1 : 0,
    transform: animate ? 'translateY(0)' : 'translateY(-100%)',
    from: { opacity: 0, transform: 'translateY(-100%)' },
    delay,
  });

  return (
    <Component className={className} style={style}>
      {animation.map((springProps, index) => (
        <TrailItem
          className="trail-item"
          key={index}
          style={springProps}
          isSpace={trailItems[index] === ' '}
        >
          {trailItems[index]}
        </TrailItem>
      ))}
    </Component>
  );
};

export default FallingTrail;
