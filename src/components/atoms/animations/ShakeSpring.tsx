import React from 'react';
import styled from 'styled-components';
import { animated, useSpring } from 'react-spring';

const Wrapper = styled(animated.div)``;

export type ShakeSpringProps = {
  className?: string;
  style?: any;
  children: React.ReactNode;
  animate?: boolean;
  delay?: number;
};

const ShakeSpring = ({
  className,
  style,
  children,
  animate = true,
  delay,
}: ShakeSpringProps): JSX.Element => {
  const animation = useSpring({
    config: { duration: 200 },
    to: async (next: CallableFunction) => {
      if (animate) {
        await next({ transform: 'rotate(15deg)' });
        await next({ transform: 'rotate(-15eg)' });
        await next({ transform: 'rotate(0eg)' });
      }
    },
    from: { transform: 'rotate(0deg)' },
    delay,
  });

  return (
    <Wrapper className={className} style={{ ...animation, ...style }}>
      {children}
    </Wrapper>
  );
};

export default ShakeSpring;
