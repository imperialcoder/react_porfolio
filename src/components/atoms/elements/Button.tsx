import React, { useState, useContext } from 'react';
import { animated, useSpring } from 'react-spring';
import styled, { ThemeContext } from 'styled-components';

import Icon from 'atoms/elements/Icon';

const Component = styled(animated.button)`
  display: flex;
  align-items: center;
  justify-content: center;
  flex-wrap: wrap;
  width: max-content;
  height: max-content;
  max-width: 20rem;
  padding: 0.5em 1rem;
  border-radius: 1rem;
  border: none;
  cursor: pointer;
  outline: none;
  font-family: ${(props) => props.theme.font.sanSerif};
  font-size: 1.1rem;
  color: ${(props) => props.theme.color.primary[1]};
  background: ${(props) => props.theme.color.text};

  i {
    margin-right: 0.5rem;
  }
`;

export type ButtonProps = {
  iconClass?: string;
  className?: string;
  children?: React.ReactNode;
  onClick?: (event: React.MouseEvent) => void;
};

const Button = (props: ButtonProps): JSX.Element => {
  const [clicked, setClicked] = useState(false);

  const themeContext = useContext(ThemeContext);

  const mouseEventHandler = (event: any) => {
    switch (event.type) {
      case 'click':
        setClicked(true);
        props.onClick && props.onClick(event);
        break;
      default:
        break;
    }
  };

  const clickedSpring = useSpring({
    config: { duration: 250 },
    transform: clicked ? 'scale(0.95)' : 'scale(1)',
    boxShadow: clicked ? '0 0 0 transparent' : themeContext.raised,
    background: clicked
      ? themeContext.color.primary[1]
      : themeContext.color.text,
    color: clicked
      ? themeContext.color.primary[3]
      : themeContext.color.primary[1],
    from: {
      transform: 'scale(1)',
      boxShadow: themeContext.raised,
    },
    onRest: () => setClicked(false),
  });

  return (
    <Component
      className={props.className}
      style={clickedSpring}
      onClick={mouseEventHandler}
    >
      {props.iconClass && <Icon iconName={props.iconClass} />}
      {props.children}
    </Component>
  );
};

export default Button;
