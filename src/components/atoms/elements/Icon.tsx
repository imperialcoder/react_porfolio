import React from 'react';
import styled from 'styled-components';

const Component = styled.i``;

export type IconProps = {
  iconName: string;
  className?: string;
  style?: any;
};

const Icon = (props: IconProps): JSX.Element => {
  return (
    <Component
      className={[props.iconName, props.className].join(' ')}
      style={props.style}
    />
  );
};

export default Icon;
