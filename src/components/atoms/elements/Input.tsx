import React from 'react';
import styled from 'styled-components';
import { animated, useSpring } from 'react-spring';

const Component = styled(animated.input)`
  background: ${(props) => props.theme.color.background[2]};
  color: ${(props) => props.theme.color.text};
  font-family: ${(props) => props.theme.font.sanSerif};
  border: none;
  border-radius: 1rem;
  font-size: 1.1rem;
  padding: 0.5em 1rem;
  outline: none;
  margin-bottom: 1rem;
`;

export type InputProps = {
  placeholder?: string;
};

const Input = ({ placeholder = 'input' }: InputProps): JSX.Element => {
  const inputSpring = useSpring({});

  return <Component style={inputSpring} placeholder={placeholder} />;
};

export default Input;
