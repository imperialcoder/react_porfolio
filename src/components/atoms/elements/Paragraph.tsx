import React from 'react';
import styled from 'styled-components';

const Component = styled.p`
  display: flex;
  font-size: 1rem;
  margin: 0.5rem 0;
  line-height: 1.5rem;
  max-width: 50vw;

  ${(props) => props.theme.media.phone} {
    max-width: 100%;
  }
`;

export type ParagraphProps = {
  children: React.ReactNode;
  maxWidth?: string;
  style?: any;
  className?: string;
};

const Paragraph = ({
  className,
  style,
  children,
}: ParagraphProps): JSX.Element => {
  return (
    <Component className={`paragraph ${className}`} style={style}>
      {children}
    </Component>
  );
};

export default Paragraph;
