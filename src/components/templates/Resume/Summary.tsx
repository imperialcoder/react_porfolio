import React from 'react';
import styled from 'styled-components';

import Section from './GridItem';
import SubTitle from 'atoms/SubTitle';
import Paragraph from 'atoms/elements/Paragraph';

const Component = styled(Section)`
  grid-column: 1 / span 3;
`;

const Info = styled(Paragraph)`
  margin: 0;
`;

const Summary: FunctionComponent = () => {
  return (
    <Component>
      <SubTitle>SUMMARY</SubTitle>
      <Info>
        Full-stack Developer who is detailed-oriented and specialized in
        creating interactive Websites using React + NodeJS
      </Info>
    </Component>
  );
};

export default Summary;
