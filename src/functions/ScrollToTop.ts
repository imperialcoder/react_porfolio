import { useLocation } from 'react-router';
import { useEffect } from 'react';

export default function ScrollToTop(): null {
  const { pathname } = useLocation();

  useEffect(() => {
    document.getElementById('root')?.scrollTo(0, 0);
  }, [pathname]);

  return null;
}
