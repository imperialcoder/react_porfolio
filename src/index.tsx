import React, { createRef } from 'react';
import { render } from 'react-dom';

import './store/store.ts';

import { BrowserRouter, Switch, Route } from 'react-router-dom';
import styled, { ThemeProvider } from 'styled-components';
import loadable from '@loadable/component';

import ScrollToTop from 'functions/ScrollToTop';
import Theme from 'styles/Theme';

import MenuBar from 'organisms/MenuBar';
import ScrollIndicator from 'atoms/ScrollIndicator';
import Footer from 'organisms/Footer';

const HomePage = loadable(() => import('pages/HomePage'));
const ErrorPage = loadable(() => import('pages/ErrorPage'));
const AboutPage = loadable(() => import('pages/AboutPage'));
const ResumePage = loadable(() => import('pages/ResumePage'));
const ContactPage = loadable(() => import('pages/ContactPage'));
const ProjectsPage = loadable(() => import('pages/ProjectsPage'));

const RootComponent = styled.div`
  display: block;
  overflow-x: hidden;
  overflow-y: scroll;
  min-height: 100vh;
  max-width: 100vw;
  max-height: 100vh;
  color: ${(props) => props.theme.color.text};
  background: ${(props) => props.theme.color.background[1]};
  font-family: ${(props) => props.theme.font.sanSerif};
  font-size: 16px;
`;

const PageContainer = styled.div`
  min-height: 100vh;
`;

const Index = () => {
  const rootCompRef: React.Ref<never> = createRef();

  return (
    <BrowserRouter>
      <ThemeProvider theme={Theme}>
        <RootComponent id="root" ref={rootCompRef}>
          <MenuBar />
          <PageContainer>
            <ScrollToTop />
            <Switch>
              <Route path="/" exact component={HomePage} />
              <Route path="/about" component={AboutPage} />
              <Route path="/projects" component={ProjectsPage} />
              <Route path="/resume" component={ResumePage} />
              <Route path="/contact" component={ContactPage} />
              <Route path="/*" component={ErrorPage} />
            </Switch>
          </PageContainer>
          <ScrollIndicator />
          <Footer />
        </RootComponent>
      </ThemeProvider>
    </BrowserRouter>
  );
};

render(<Index />, document.getElementById('root'));
