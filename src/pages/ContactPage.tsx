import React from 'react';
import styled from 'styled-components';

import PageSegment from 'atoms/PageSegment';
import Title from 'atoms/elements/Title';
import Button from 'atoms/elements/Button';
import TextArea from 'atoms/elements/TextArea';
import Input from 'atoms/elements/Input';

const Form = styled.div`
  display: flex;
  flex-direction: column;
  width: 30rem;
  margin-top: 2rem;
  ${(props) => props.theme.media.phone} {
    width: 100%;
  }
`;

const CenteredPageSegment = styled(PageSegment)`
  justify-content: center;
  align-items: center;
`;

const ContactPage: FunctionComponent = () => {
  return (
    <>
      <CenteredPageSegment>
        <Title>Contact</Title>
        <Form>
          <Input placeholder="Email" />
          <Input placeholder="Subject" />
          <TextArea placeholder="Message" />
          <Button>Submit</Button>
        </Form>
      </CenteredPageSegment>
    </>
  );
};

export default ContactPage;
