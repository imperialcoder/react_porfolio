import React from 'react';
import styled from 'styled-components';
import { useHistory } from 'react-router-dom';

import Title from 'atoms/elements/Title';

const Component = styled.div`
  top: 0;
  position: absolute;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  width: 100vw;
  height: 100vh;
  background: ${(props) => props.theme.color.background[1]};
  z-index: 101;
`;

const Button = styled.button``;

const ErrorPage: FunctionComponent = () => {
  const history = useHistory();
  return (
    <Component>
      <Title>Oh no! an Error Occurred!</Title>
      <Button onClick={() => history.push('')}>Return Home</Button>
    </Component>
  );
};

export default ErrorPage;
