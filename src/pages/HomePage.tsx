import React from 'react';

import PageSegment from 'atoms/PageSegment';
import Title from 'atoms/elements/Title';
import Paragraph from 'atoms/elements/Paragraph';

import FallingTrail from 'atoms/animations/FallingTrail';
import FadeSpring from 'atoms/animations/FadeSpring';

const HomePage = (): JSX.Element => {
  return (
    <>
      <PageSegment>
        <Title>
          <FallingTrail>Hello, my name is</FallingTrail>
          <FallingTrail delay={750} style={{ color: 'red' }}>
            {[' ', 'J', 'e', 's', 's', 'e', '.']}
          </FallingTrail>
        </Title>
        <Title>
          <FallingTrail delay={1500}>I'm a Software Engineer.</FallingTrail>
        </Title>
        <Paragraph>
          <FadeSpring delay={2500} swipe="top">
            A beer & bicycle loving Programmer who lives in Portland, Oregon
          </FadeSpring>
        </Paragraph>
      </PageSegment>

      <PageSegment>
        <Title>What I can do for you?</Title>
        <Paragraph>
          I'm a full-stack Engineer who can Manage, Host, and Create Blazing
          Fast, Secure, and Fluid Websites for your Businesses! From the
          front-end of your Applications, to Scaling + Securing your back-end,
          to Managing and Deploying all of it, I have your back!
        </Paragraph>
      </PageSegment>
    </>
  );
};

export default HomePage;
