import React from 'react';
import { useRef } from 'react';
import styled from 'styled-components';

import Header from 'templates/Resume/Header';
import Summary from 'templates/Resume/Summary';
import Skillz from 'atoms/Skillz';

import PageSegment from 'atoms/PageSegment';
import Title from 'atoms/elements/Title';

const Component = styled.div`
  display: flex;
  flex: 1;
  max-width: 65rem;
  min-width: 44rem;
  margin: 0 auto;

  ${(props) => props.theme.media.phone} {
    min-width: 0;
  }

  ul {
    margin-top: 0;
  }

  @media print {
    padding: 0;
  }
`;

const SubTitle = styled(Title)`
  font-size: 1.8rem;
  justify-content: center;
`;

const PageGrid = styled(PageSegment)`
  display: grid;
  grid-gap: 1rem;
  grid-template-columns: repeat(3, 1fr);
  grid-template-rows: auto;
  padding: 2rem;

  ${(props) => props.theme.media.phone} {
    padding: 0;
    grid-template-columns: auto;
  }

  @media print {
    grid-gap: 0.5rem;
  }
`;

const GridItem = styled.div`
  display: flex;
  flex-direction: column;
  background: ${(props) => props.theme.color.background[2]};
  padding: 1rem;
  border-radius: 1rem;
`;

const Experience = styled(GridItem)`
  grid-column: 1 / span 3;
`;

const Education = styled(GridItem)`
  grid-column: 1 / span 3;
`;

const ResumePage: FunctionComponent = () => {
  const componentRef: any = useRef();

  return (
    <Component ref={componentRef}>
      <PageGrid>
        <Header printRef={componentRef} />
        <Summary />
        <Skillz title="FRONT-END SKILLS">
          HTML CSS SASS LESS PostCSS Javascript Typescript JSX/TSX Angular React
          Webpack Jasmine Jest Selenium Redux Ngrx-store RXJS
        </Skillz>
        <Skillz title="BACK-END SKILLS">
          Docker Kubernetes AWS Azure GCP Linode MongoDB MariaDB PostgreSQL
          NGINX NodeJS Express C# Java PHP
        </Skillz>
        <Skillz title="OTHER SKILLS">Linux Git Agile-Methodology</Skillz>

        <Experience>
          <SubTitle>EXPERIENCE</SubTitle>
          <div>
            <div>Natures Sunshine - Full-stack Web Developer</div>
            <div>October 2018 - July 2020</div>
            <ul>
              <li>
                Developed business side tools using Angular, React, and
                dot-net-core
              </li>
              <li>
                Managed and deployed services using Docker in Azure and GCP
              </li>
              <li>
                Responsible for helping create qemp.com and naturessunshine.com
              </li>
              <li>
                Excelled at making clean and functional UI/UX components/pages
              </li>
            </ul>
          </div>

          <div>
            <div>Target Local Marking - PHP Developer</div>
            <div>May 2017 - July 2017</div>
            <ul>
              <li>
                Optimized existing pieces of PHP code for clients projects
              </li>
              <li>Transfer Domains to new service providers for clients</li>
              <li>Transitioned websites from staging to production</li>
            </ul>
          </div>
        </Experience>

        <Education>
          <SubTitle>EDUCATION</SubTitle>
          <div>
            <div>MTECH - Web Programming & Development</div>
            <div>January 2018 - December 2018</div>
            <ul>
              <li>
                Hands-on instruction by full-time industry professionals; namely
                web developers, software engineers, security specialists,
                technology consultants, program managers, and human resource
                specialists.
              </li>
              <li>
                Gained invaluable knowledge and experience developing with HTML,
                CSS, LESS, JavaScript, TypeScript, Angular, and React. Other
                topics include Git, Automated QA, Back-End, Database, Hosting,
                Deployment, Security, and Soft Skills.
              </li>
            </ul>
          </div>
        </Education>
      </PageGrid>
    </Component>
  );
};

export default ResumePage;
